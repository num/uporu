---
author: Votre nom
title: 👏 Crédits
---

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/), et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/) pour la partie Python nécessaire pour les QCM.


