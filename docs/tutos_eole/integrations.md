---
author: Collège Uporu
title: Intégration des postes au domaine
tags:
    - intégration
    - domaine
    - Eole
    - Windows
    - Linux
---

## Où trouver la documentation EOLE

La documentation concernant le serveur Eole 2.8 est assez complète mais [il peut être difficile de s'y retrouver entre les différentes générations de documents proposés](https://eole.ac-dijon.fr/documentations//2.8/completes/HTML/ModuleScribe/co/ModuleScribe.html).

Concernant l'intégration des postes nous avons les documentations suivantes :

- [Intégration des postes MS Windows](https://eole.ac-dijon.fr/documentations//2.8/completes/HTML/ModuleScribe/co/00-ClientEOLE.html)
- [Intégration des postes sous GNU/Linux](https://eole.ac-dijon.fr/documentations//2.8/completes/HTML/ModuleScribe/co/01-ClientsLinux.html)

