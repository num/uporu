---
author: Collège Uporu
title: Découverte de Linux
tags:
    - Linux
    - PrimTux
    - LinuxMint
    - découverte
    - présentation
---


## Découvrir PrimTux 

PrimTux est une distribution GNU/Linux qui comprends de très nombreux logiciels classés selon les programmes officiels d'enseignement des cycles 1 à 3. La consultation de ces ressources est susceptible de présenter un intérêt pour les professeurs de collège qui auraient besoin d'applications ludo-pédagogiques ciblées pour des élèves à besoins particuliers.

La vidéo suivante décrit l'ensemble de PrimTux en 2 minutes. Mais pour de plus amples informations vous pouvez vous diriger vers [le site officiel et sa documentation](https://primtux.fr/) (en français) qui sont très pertinents et complets.

<video controls src="https://primtux.fr/ptx8.mp4"></video>



## Découvrir Linux Mint Xfce edition

Ces quelques images présentent Linux Mint qui est un système d'exploitation générique. Vous pouvez également consulter le [site internet de Linux Mint](https://www.linuxmint.com/) (en anglais).

![](./img/mint01.png)
![](./img/mint02.png)
![](./img/mint03.png)
![](./img/mint04.png)
![](./img/mint05.png)
![](./img/mint06.png)
![](./img/mint07.png)
![](./img/mint08.png)
![](./img/mint09.png)
![](./img/mint10.png)
![](./img/mint11.png)
![](./img/mint12.png)


