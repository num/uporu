---
author: Collège Uporu
title: Installer la distribution Debian 12
tags:
    - debian
    - Linux
    - OS
    - distribution
---

Debian est une distribution intéressante car elle est relativement bien documentée pour son intégration sur les réseaux pédagogiques tels qu'Eole (et SambaEdu).

Nous choisissons d'installer la distribution DebianFacile 12 (parfum XFCE) sur 6 ordinateurs portables i3 avec 4Go de RAM et un disque HDD.

!!! warning "suivre les étapes dans l'ordre"
    Ce tutoriel est à suivre dans l'ordre des étapes car certains prérequis sont vus dans des étapes précédentes (par exemple, il convient de passer le proxy tel que décrit dans l'étape de la "mise à jour de l'OS" avant de pouvoir installer des logiciels supplémentaires)

!!! info "la doc officielle de Debian Facile est très bien faite pour les débutants"    
    Je vous conseille de consulter [la doc officielle de Debian Facile pour le téléchargement, l'installation et l'utilisation de la distribution](https://debian-facile.org/projets:iso-debian-facile#telecharger). Si vous avez un doute ou un point bloquant à la lecture de cette simple page de tutoriel, nul doute que la lecture de la page de documentation officielle vous débloquera de cette situation.

## Récupérer l'image iso de Debian Facile 12

La page de téléchargement de Debian sur le site de DebianFacile explique très bien les raisons qui nous ont fait choisir cette image iso pour l'installation :

!!!info "Des membres de la communauté ont développé une ISO Debian-Facile, une Debian XFCE personnalisée, assortie de quelques outils facilitants originaux, comme un menu simplifié, ainsi qu'une documentation PDF pour débutants intégrée. Cette image Live est tout à fait adaptée à l’installation et a l’avantage d’inclure les firmwares non-free et un sources.list comprenant les sections main contrib et non-free ce qui facilite l’installation et la prise en main immédiate par les débutants."

Télécharger l'[image iso (64 bits) de la DebianFacile directement sur le site de l'association](https://debian-facile.org/dflinux/isos/)


## Créer la clé USB bootable à partir de l'image iso

!!! tip "Créer une clé USB bootable en moins de 4 minutes"

    <iframe title="Création d'une clé USB de boot GNU/Linux" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/44b430b4-a0a4-4246-ba0a-771a78b68025" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>



Je vous conseille Etcher pour créer la clé bootable. Toutes les explications pour créer sa clé USB bootable avec Balena Etcher sont sur [la page dédiée aux clés USB bootables avec Etcher ou Ventoy](../tutos/cle_boot.md). Et [un tuto illustré spécifique à Etcher est disponible sur la page liée à l'installation de Linux Mint](../chapitre_mint/chapitre_mint_page1.md#telecharger-et-installer-balena-etcher)

## Essayer DebianFacile avant de l'installer

Lors du boot, choisir "**Tester Debian 12**" pour tester la distribution en "live" puis pour l'installer le cas échéant (étape suivante de ce tutoriel).

![](./img/lancement_USB_DF.png)

L'image iso téléchargée et placée sur clé USB bootable est une distribution live installable. Cela signifie que l'on peut tout d'abord essayer cette distribution sans risque depuis la clé USB avant de l'installer sur le disque dur de l'ordinateur.

!!! tip "Il faut booter sur la clé USB pour lancer le chargement de l'image iso." 
    Il est possible que vous ayiez [quelques réglages à faire dans le BIOS de la machine](../chapitre_mint/chapitre_mint_page1.md#acceder-au-bios-de-la-machine) comme [activer le menu de boot](../chapitre_mint/chapitre_mint_page1.md#activer-le-boot-menu) ou [désactiver le secure boot](../chapitre_mint/chapitre_mint_page1.md#desactiver-le-secure-boot), mais cela n'est pas systématique.

![](./img/premier_lancement_DebianFacile.png)    
 
## Installer DebianFacile

L'installation est triviale.
Voici les quelques captures où il y a quelque chose de particulier à saisir ou cocher (on peut laisser tous les autres réglages par défaut).

Cliquer sur l'icone ad-hoc qui est présent à gauche du bureau lors du lancement de la distribution live :

![](./img/icone_install_live.png)

On sélectionne le bon fuseau horaire :
![](./img/df_tahiti.png)

On choisi une installation sur tout le disque dur (cela permet de virer des partitions inutiles) :
![](./img/choix_disque_DF_install.png)

Comme il s'agit de déployer des postes de classe mobile, on les a appelé MOBI-P01, MOBI-P02... pour se garder la possibilité de les intégrer au réseau ultérieurement :
![](./img/df_prof.png)

## Accéder à ce tutoriel depuis Firefox pour permettre les copier-coller

!!! warning "Réglage du proxy dans Firefox, le cas échéant"

    Une fois connecté au compte prof, pour accéder à ce tutoriel, il faut d'abord permettre à Firefox de passer le proxy (10.21.0.1:3128)

    ![](./img/proxy_firefox.png)

    Puis accéder à l'adresse générique du site de documentation RUPN du collège : [https://num.forge.apps.education.fr/uporu](https://num.forge.apps.education.fr/uporu). Ce tutoriel se trouve dans la rubrique "**Tutoriels Linux**".

## Mises à jour du système

!!! warning "permettre au gestionnaire de paquet apt de passer le proxy de l'établissement"

    Le cas échéant, il faut éditer le fichier `/etc/apt/apt.conf`

    ```bash
    sudo nano /etc/apt/apt.conf
    ```

    et y ajouter les deux lignes suivante :

    ```
    Acquire::http::proxy "http://10.21.0.1:3128/";
    Acquire::ftp::proxy "ftp://10.21.0.1:3128/";
    ```

      

On met à jour le système en ouvrant une console texte (Système / Terminal XFCE) :
```bash
sudo apt update
sudo apt upgrade
```

## Installer quelques logiciels supplémentaires

On va avoir besoin de Gimp et Shotcut qui sont exigés pour PIX. On en profite pour installer une dépendance de libreoffice nécessaire pour installer ensuite des extensions de correction grammaticale.

```bash
sudo apt install gimp shotcut libreoffice-java-common
```

Si on le souhaite, on peut installer ensuite [Geogebra 5](install_geogebra_5.md) et [Scratch 3 ](install_scratch_3.md) pour pouvoir les utiliser hors ligne.

!!! warning "paquet nécessaire pour la distri MX-Linux"
    MX-Linux s'installe de la même manière que la DebianFacile 12 mais on aura besoin d'installer un autre paquet pour intégrer l'extension grammalecte dans libreoffice :
    ```bash
    sudo apt install libreoffice-script-provider-python
    ```


## Créer un compte eleves/eleves

On lance l'outil "Utilisateurs et groupes" pour ajouter un nouvel utilisateur (sans droits d'administration du système).

On notera que le mot de passe comporte 7 caractères, car Debian n'autorise pas les mots de passe  de taille inférieure. Nous avons donc opté pour eleves/eleves pour le couple identifiant/mot de passe du compte pour les élèves.
![](./img/df_eleves.png)

## Installer des extensions dans libreoffice sur le compte eleves

### Accéder à ce tutoriel depuis Firefox pour permettre les copier-coller

!!! warning "Réglage du proxy dans Firefox, le cas échéant"

    Une fois connecté au compte eleves, pour accéder à ce tutoriel, il faut d'abord permettre à Firefox de passer le proxy (10.21.0.1:3128)

    ![](./img/proxy_firefox.png)

    Puis accéder à l'adresse générique du site de documentation RUPN du collège : [https://num.forge.apps.education.fr/uporu](https://num.forge.apps.education.fr/uporu). Ce tutoriel se trouve dans la rubrique "**Tutoriels Linux**".


### puis réaliser les intégrations d'extensions suivantes.

Ces extensions s'avèrent très pertinentes, surtout pour les rédactions des élèves sur traitement de texte. Evidemment, il faudra signaler aux élèves l'existence de ces nouvelles propositions de correction.

Installer les extensions [grammalecte](https://extensions.libreoffice.org/fr/extensions/show/grammalecte) et [languagetool](https://extensions.libreoffice.org/fr/extensions/show/languagetool) (pour tous les utilisateurs de l'ordinateur).
Pour cela, aller sur [extensions.libreoffice.org](https://extensions.libreoffice.org) et télécharger les fichiers en question puis les installer en les ouvrant (fichier/ouvrir) dans libreoffice.

![](./img/install_languagetool.png)


## En cas d'erreurs à l'affichage

Des soucis liés au BIOS peuvent apparaître au démarrage de la machine :
![](./img/erreur_debianfacile12.png)
Une recherche sur internet peut donner [des axes de solution](https://forums.linuxmint.com/viewtopic.php?t=339223)
