---
author: Collège Uporu
title: Installer Geogebra 5 sous Linux
tags:
    - geogebra
    - Linux
    - raccourci
    - lien relatif
---


Geogebra 5 est très utilisé en collège. Il ne bénéficie plus de binaire installable mais exclusivement d'une version portable (qui ne nécessite pas d'installation). Mais qui dit "pas d'installation" dit aussi "pas de raccourci pour le lancer". Nous allons remédier à ce léger manque.

Il existe bien geogebra dans les dépôts debian mais c'est la version 4 qui est particulièrement ancienne.

L'installation de Geogebra 5 se déroule en trois étapes :

- Installation de geogebra pour tous les comptes de l'ordi (professeurs et élèves) ;
- mise en place d'un lien pour lancer geogebra depuis une console ;
- création d'un raccourci dans le menu

## Installation de Géogebra 5

Nous allons commencer par récupérer la dernière version portable de Geogebra 5 pour Linux, directement sur le [dépôt Github de Geogebra](https://geogebra.github.io/docs/reference/en/GeoGebra_Installation/). Lors de la rédaction de ce tutoriel, le dernier fichier en date était [accessible à cette adresse](https://download.geogebra.org/package/linux-port).

Ensuite nous ouvrons une console texte (Dans Système/Terminal Xfce dans le cas de la distribution Debian Xfce par exemple).

Nous saisissons la commande suivante qui va décompresser l'archive téléchargée et la placer dans le répertoire /opt/ (corriger, le cas échéant, le nom du fichier par le vôtre) :
```bash
sudo tar -xf ~/Téléchargements/GeoGebra-Linux-Portable-5-2-861-0.tar.bz2 -C /opt/
```

## Création du lien 
Toujours dans la console texte, créer un lien relatif permettant de saisir directement le terme "geogebra" dans une console texte pour lancer l'application :
```bash
sudo ln -s /opt/GeoGebra-Linux-Portable-5-2-861-0/geogebra-portable /usr/local/bin/geogebra
```

## Création d'un raccourci
Dans le terminal ouvrir l'éditeur nano et créer le fichier geogebra.desktop en saisissant :
```bash
sudo nano /usr/share/applications/geogebra.desktop
```

et y placer le contenu suivant (l'adresse de l'icone est ici contextualisée à une Debian 12) :
```bash
[Desktop Entry]
Version=1.1
Type=Application
Terminal=false
Name=Geogebra 5
GenericName=Geogebra
Comment=Logiciel interactif de géométrie, algèbre
Exec=geogebra
Icon=/var/lib/swcatalog/icons/debian-bookworm-main/64x64/geogebra_geogebra.png
Categories=Education;
```

Il suffit de se déconnecter puis de se reconnecter pour avoir l'icone de geogebra dans la catégorie Education 

