---
author: Collège Uporu
title: Client Pronote sous Linux
tags:
    - pronote
    - Linux
    - Wine
---

![icône de pronote client](./img/pronote_sur_linux_mint.png)

Le client Pronote est un logiciel prévu pour tourner sous MS Windows. Il est tout à fait possible de le faire fonctionner sous Linux moyennant quelques manipulations.

L'installation du client Pronote 2023 en trois étapes :

- Installation générique de wine (à ne faire qu'une fois pour toutes) ;
- configuration de Wine (à ne faire qu'une fois pour toutes) ;
- Installation du client Pronote dans Wine et lanceur sur le bureau (à reproduire chaque année, à chaque nouvelle version du client).

## Installation de Wine

installer WineHQ-stable tel que cela est présenté dans la documentation officielle de WineHQ

!!! tip "installation hors ligne"

    Il est possible d'installer wine si la machine n'a pas d'accès internet ou si les caractéristiques du réseau vous en empêchent (proxy difficile à passer...). Pour cela il faut suivre la procédure suivante : [https://wiki.winehq.org/Ubuntu#Installing_without_Internet](https://wiki.winehq.org/Ubuntu#Installing_without_Internet). Dans la documentation de wine il est proposé d'utiliser une image virtualisée de la même distribution ; je vous conseille `Gnome-Boxes` (appelé également `Machines`) : [https://wikilibriste.fr/tutoriels/gnome-boxes](https://wikilibriste.fr/tutoriels/gnome-boxes).
    
### Activer l'architecture 32bits
```bash
sudo dpkg --add-architecture i386
```

### Installer "winehq.key" 
```term
sudo mkdir -pm755 /etc/apt/keyrings

sudo wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
```

### Installer le fichier de sources

En fonction du nom de code de votre version d'Ubuntu : ici Linux Mint 21.3 Virginia qui est construite sur une base Ubuntu Jammy 22.04 :
```bash
sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources
```
### mettre à jour la liste des paquets
```bash
sudo apt update
```

### Installer le paquet 'WineHQ Stable'

```bash
sudo apt install --install-recommends winehq-stable
```

## Configuration de Wine pour le client Pronote



Les commandes suivantes sont à saisir dans un terminal, en restant dans la même session car sinon les variables locales définies avec la commande "export" seront perdues.

### Vérification de la version de Wine
```bash
wine --version # Cela renvoie par exemple wine-9.0.
```

### Création du préfixe Wine à utiliser

C'est-à-dire : création du dossier qui contiendra le disque C vu par les applications sous Wine
```bash
export WINEPREFIX=CheminAbsoluVersVotrePrefixe # par exemple /home/ecole/.winepronote. Le dossier .winepronote ne doit pas exister, il sera créé par la commande wineboot.
wine wineboot
cd $WINEPREFIX
```

### Utilisation de WineTricks

Pour installer MsXML 6, Windows Codecs, les polices de base dans le nouveau préfixe, et changement de version du préfixe pour Windows 10
```bash
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
sh winetricks windowscodecs
sh winetricks corefonts
sh winetricks win10 
```

## Installer le client PRONOTE avec Wine

### Télécharger le client pronote

Télécharger [le client pronote 64 bits](https://www.index-education.com/fr/telecharger-pronote.php),
et placer le fichier obtenu dans le répertoire WINEPREFIX (soit /home/ecole/.winepronote)

### Installation du Client PRONOTE
```bash
WINEPREFIX=/home/ecole/.winepronote/
cd $WINEPREFIX
wine Install_xxx_win64.exe # (spécifiez le chemin au besoin ; xxx est variable en fonction de la version du client pronote : par exemple 'PRNclient_FR_2023.0.2.9')
```

## Toutes les étapes précédentes en un seul script

Pour information, ce script Bash provenant du [forum index éducation](https://forum.index-education.com/questions/9976/comment-installer-pronote2023-sous-linux?tri%C3%A9=voix&page=3#post-10712-downvote) fonctionne très bien (à condition de corriger les lignes relatives à la version du client pronote actuellement en cours) et rajoute les liens et raccourcis fonctionnels :

```bash
#!/bin/bash
#Installation wget
sudo apt update
sudo apt install wget
#Vérification wine
echo "Wine est il déjà installé (y/n)"
read Reponse
if [[ ${Reponse} == "n" ]]
then
    echo "Ajout d'une architecture 32 bits"
    sudo dpkg --add-architecture i386

    echo "Ajout clé de sécurité pour le dépot"
    sudo mkdir -pm755 /etc/apt/keyrings
    sudo wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key

    echo "Ajout du Dépot de Ubuntu ${Reponse}"
    sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources


    echo "Installation de wine"
    sudo apt update
    sudo apt install --install-recommends winehq-stable
    Reponse="y"
fi


if [[ ${Reponse} == "y" ]]
then
    #Installation winetricks
    sudo apt install cabextract
    export WINEPREFIX="$HOME/.winepronote"
    wine wineboot
    cd $WINEPREFIX
    wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
    #sh winetricks msxml6 (retiré car génère un bug)
    sh winetricks windowscodecs
    sh winetricks corefonts
    sh winetricks win10

    #Installation Pronote
    wget "https://tele7.index-education.com/telechargement/pn/v2023.0/exe/Install_PRNclient_FR_2023.0.3.0_win64.exe" -O "$WINEPREFIX/Install64.exe"
    wine Install64.exe

    #Création lanceur personnalisé
    echo '#!/bin/bash' >> LancerPronote.sh
    echo "export WINEPREFIX=$WINEPREFIX" >> LancerPronote.sh
    echo 'sh -c `rm -r "$WINEPREFIX/drive_c/ProgramData/IndexEducation/PRONOTE/CLIENT/VERSION 2023-0/FR/Cache"; env WINEPREFIX="$WINEPREFIX" wine C:\\\\ProgramData\\\\Microsoft\\\\Windows\\\\Start\\ Menu\\\\Programs\\\\PRONOTE\\ Réseau\\ 2023\\\\Client\\ PRONOTE\\ 2023.lnk`' >> LancerPronote.sh
    chmod +x LancerPronote.sh

    #Suppression raccourci wine
    rm "$HOME/.local/share/applications/wine/Programs/PRONOTE Réseau 2023/Client PRONOTE 2023.desktop"

    #Création Raccourci
    wget "https://www.index-education.com/contenu/img/fr/PRONOTE_Installer_client_2023.png" -O "$WINEPREFIX/Icon.png"
    Raccourci="$HOME/.local/share/applications/Pronote.desktop"
    echo '[Desktop Entry]' >> ${Raccourci}
    echo 'Name=Pronote' >> ${Raccourci}
    echo 'GenericName=Pronote' >> ${Raccourci}
    echo 'Comment=Pronote' >> ${Raccourci}
    echo "Exec=$WINEPREFIX/LancerPronote.sh %F" >> ${Raccourci}
    echo 'Terminal=false' >> ${Raccourci}
    echo 'Type=Application' >> ${Raccourci}
    echo "Icon=$WINEPREFIX/Icon.png" >> ${Raccourci}

    #Nettoyage
    echo "Suppression du fichier exécutable de pronote"
    rm Install64.exe
fi
```






