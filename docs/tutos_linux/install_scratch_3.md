---
author: Collège Uporu
title: Installer Scratch 3 sous Linux
tags:
    - scratch
    - Linux
---


Scratch 3 est très utilisé en collège. Une ancienne version est présente dans les dépôts de Debian mais nous avons besoin de la version 3 pour les formations en maths et en technologie.
L'installation de Scratch 3 se fait via le paquet scratch-desktop 3 et se déroule en une seule étape :

- Installation du binaire .deb de scratch 3

## Installation de Scratch 3

Télécharger la [dernière release de Scratch-desktop 3](https://github.com/redshaderobotics/scratch3.0-linux/releases/).
Puis ouvrir un terminal (Système / Terminal Xfce dans Debian XFCE)

```bash
sudo apt install ~/Téléchargements/scratch-desktop_3.3.0_amd64.deb
```

