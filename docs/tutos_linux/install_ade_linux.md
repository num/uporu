---
author: Collège Uporu
title: Adobe Digital Edition (ADE) sous Linux
tags:
    - ADE
    - Adobe Digital Edition
    - Linux
    - Wine
---

ADE peut être utile pour pouvoir lire des documents (livres numrériques essentiellement) qui seraient détenteurs d'un verrou numérique (verrours DRM).

Il est bien entendu conseillé de ne télécharger que des ouvrages sans ces verrous DRM pour les utiliser directement dans `Calibre Ebook` par exemple.

L'installation d'ADE se déroule en trois étapes :

- Installation générique de wine (à ne faire qu'une fois pour toutes ; cette étape n'est même pas nécessaire si vous avez déjà wine d'installé) ;
- configuration de Wine (à ne faire qu'une fois pour toutes) ;
- Installation d'ADE dans Wine et configuration éventuelle du lanceur sur le bureau (à reproduire si besoin lors de la sortie d'une nouvelle version d'ADE, ce qui est rare).

## Installation de Wine

Pour installer Wine, vous pouvez utiliser la méthode déjà présentée dans le [tutoriel sur l'installation de Pronote](./install_pronote_linux.md)

## Configuration de Wine pour ADE



Les commandes suivantes sont à saisir dans un terminal, en restant dans la même session car sinon les variables locales définies avec la commande "export" seront perdues.

Les informations données ici proviennent de la [base de connaissance de WineHQ](https://appdb.winehq.org/objectManager.php?sClass=version&iId=33276).

### Vérification de la version de Wine
```bash
wine --version # Cela renvoie par exemple wine-9.0.
```

### Création du préfixe Wine à utiliser

C'est-à-dire : création du dossier qui contiendra le disque C vu par les applications sous Wine
```bash
export WINEPREFIX=CheminAbsoluVersVotrePrefixe # par exemple /home/ecole/.wineade   . Le dossier .wineade ne doit pas exister, il sera créé par la commande wineboot.
wine wineboot
cd $WINEPREFIX
```

### Utilisation de WineTricks

Pour installer donet40, les polices de base dans le nouveau préfixe, et changement de version du préfixe pour Windows 10
```bash
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
sh winetricks donet40
sh winetricks corefonts
sh winetricks win10
```

## Installer ADE avec Wine

### Télécharger ADE

Télécharger [ADE en version MS Windows](https://www.adobe.com/solutions/ebook/digital-editions/download.html),
et placer le fichier obtenu dans le répertoire WINEPREFIX (soit /home/ecole/.wineade)

### Installation d'ADE
```bash
WINEPREFIX=/home/ecole/.wineade/
cd $WINEPREFIX
wine ADE_x.x_Installer.exe # (spécifiez le chemin au besoin ; xxx est variable en fonction de la version d'ADE : par exemple 'ADE_4.5_Installer.exe')
```

