---
author: Collège Uporu
title: Démonstrations
tags:
    - démonstration
    - PrimTux
    - LinuxMint
    - Linux
    - proxy
---

## Deux Linux = 2 ordis

Deux distributions GNU/linux sont proposées en démonstration sur deux ordinateurs distincts. Ainsi les professeurs du collège pourront découvrir deux environnements différents et des logiciels pédagogiques. Les professeurs des établissements primaires pouvant faire l'objet de donations par le collège pourront également expérimenter les deux environnement et choisir ce qui leur sera le plus adapté selon la destination finale du poste.

Pour simplifier la connexion au poste sous LinuxMint, il a été sélectionné un démarrage automatique sans avoir besoin de saisir le mot de passe (dans notre cas de figure, le couple login/mdp temporairement choisi pour ces démonstrations est `ecole/ecole24` mais il ne servira que pour l'administrateur afin, éventuellement, d'installer de nouveaux logiciels)


### Configurer le proxy pour naviguer sur internet

Dans Mozilla Firefox, il faut aller dans les `Paramètres` puis `Paramètres réseau` et enfin cocher `Détection automatique des paramètres de proxy pour ce réseau`

![](./img_mint/firefox_proxy.png)


### Configurer le proxy pour télécharger des logiciels

Il faut éditer le fichier `/etc/apt/apt.conf`

```bash
sudo nano /etc/apt/apt.conf
```

et y ajouter les deux lignes suivante :

```
Acquire::http::proxy "http://<adresseIP>:<portIP>/";
Acquire::ftp::proxy "ftp://<adresseIP>:<portIP>/";
```

`<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.21.0.1:3128`

![](./img_mint/ajout_IP_Proxy.png)

## Affichage

[Les affiches pour les ordis de démonstration](./img_mint//ordinateurs_demonstration.pdf){ .md-button target="_blank" rel="noopener" }

## Logiciels supplémentaires à installer dans LinuxMint

L'ordinateur sous Linux Mint est relativement pauvre en logiciels suite à l'installation.
Essentiellement on y trouve la suite bureautique LibreOffice et le couple Firefox / Thunderbird.

Mais la liste des logiciels installables est nettement plus importante que les besoins des utilisateurs.

Pour permettre aux professeurs de tester davantage d'outils, nous avons choisi d'installer, par exemple, les logiciels suivants :

- gimp
- inkscape
- SweetHome3D
- FreeCad
- shotcut
- pdf4teachers
- xournal++
- freeplane
- logiquiz
- optimoffice
- canoprof
- anki
- krop

La plupart de ces logiciels s'installent facilement en une seule commande via la console du terminal ++ctrl+alt+t++, comme le montre l'exemple suivant : 

```bash
sudo apt install gimp inkscape sweethome3d freecad
```




