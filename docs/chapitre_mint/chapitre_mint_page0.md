---
author: Collège Uporu
title: Présentation
tags:
    - linux
    - découverte
    - Linux Mint
    - Xfce
    - PrimTux
---

## Reconditionner d'anciens ordinateurs sous GNU/Linux 

Des ordinateurs étaient encore récemment utilisés alors qu'ils n'étaient plus bénéficiaires des mises à jour de sécurité Microsoft (postes sous MS Windows 7 et 8 pour l'essentiel). 

Un reconditionnement est planifié sous GNU/Linux pour qu'ils puissent toujours bénéficier de logiciels récents et de toute la sécurité liée à une version récente de l'OS[^1].

Le choix s'est donc porté sur le reconditionnement de :

- Deux ordinateurs sous [LinuxMint Xfce 21.3](./chapitre_mint_page6.md)  [avec Pronote pour la salle des profs](../tutos_linux/install_pronote_linux.md) ;
- de 6 ordinateurs sous [PrimTux 8 en simplifiant le menu de démarrage](../tutos_linux/decouverte_linux_distri.md) pour les élèves à besoins particuliers ;
- de [5 ordinateurs portables sous PrimTux 8](../tutos_linux/decouverte_linux_distri.md) pour la classe ULIS ;
- les autres postes (entre 2 et 6 en fonction de l'état physique des machines) seront cédés à des structures d'enseignement public (Centre des Jeunes Adolescents notamment) en [version OEM de LinuxMint](./chapitre_mint_page2.md) ;
- Les anciens postes ne supportant que le 32 bits sont upgradés avec MX-23.3 i386 edition et également cédés à des structures d'enseignement public ;
- Les postes MS Windows (57 neufs sous W11, 6 anciens upgradés sous W11 et 9 anciens qui ne peuvent pas dépasser la version W10) sont déployés dans deux salles de classes (salle de techno et salle informatique) + 1 ordi prof relié au VP dans chacune des 16 salles de cours.

### Reconditionnements sous LinuxMint Xfce

Une **présentation de LinuxMint** est proposée sur [cette page](../tutos_linux/decouverte_linux_distri.md)

Le reconditionnement sous Linux Mint pouvant se faire en mode OEM, un tutoriel spécifique, en quatre parties, a été rédigé ici-même : 

- [Reconditionnement Linux Mint - Prérequis](./chapitre_mint_page1.md)
- [Reconditionnement Linux Mint - Installation OEM](./chapitre_mint_page2.md)
- [Reconditionnement Linux Mint - Démonstrations](./chapitre_mint_page4.md)
- [Reconditionnement Linux Mint - Premier démarrage](./chapitre_mint_page3.md)
- [Reconditionnement Linux Mint - Installation standalone de PrimTux](./chapitre_mint_page5.md)
- [Reconditionnement Linux Mint - Installation classique pour intégration au domaine](./chapitre_mint_page6.md)



### Reconditionnement sous PrimTux

Une **présentation de PrimTux** est proposée sur [cette page](../tutos_linux/decouverte_linux_distri.md)

Concernant le reconditionnement sous PrimTux nous vous renvoyons directement vers [le site officiel et sa documentation francophone](https://primtux.fr/) qui est très pertinent et complet.




[^1]: système d'exploitation
