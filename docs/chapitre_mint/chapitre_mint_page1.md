---
author: Collège Uporu
title: Prérequis
tags:
    - Linux
    - Clé de Boot
    - installation
    - Bios
---

## Matériel nécessaire

- une clé USB ;
- un ordinateur (boîtier, écran, cables d'alimentation et cable de connexion vidéo entre l'écran et le boîtier) ;
- un cable étehrnet ;

Il faut prévoir un emplacement où on peut se brancher au réseau ainsi que 2 prises électriques libres (écran + boîtier)

## Télécharger l'image disque et créer une clé USB bootable

Nous choisissons volontairement une version de Linux Mint dont le bureau est qualifié de "léger", c'est à dire qu'il n'est pas surchargé de fioritures graphiques qui risqueraient de ralentir l'affichage et le lancement des applications.

!!! tip "Créer une clé USB bootable en moins de 4 minutes"

    <iframe title="Création d'une clé USB de boot GNU/Linux" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/44b430b4-a0a4-4246-ba0a-771a78b68025" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


### Télécharger l'image iso de Linux Mint Xfce edition

[Télécharger Linux Mint Xfce édition](https://www.linuxmint.com/download.php)



![](./img_mint/download_mint.png)

### Télécharger et installer Balena Etcher

[Télécharger Balena Etcher](https://etcher.balena.io/)

![](./img_mint/download_etcher.png)

Ensuite lancer le paquet téléchargé pour l'installer sur votre ordinateur.

### Créer une clé USB bootable

Insérer une clé USB dans un port libre de l'ordinateur et lancer Balena Etcher.

Sélectionner l'image iso à utiliser (sur la capture d'écran il s'agit de la distribution `Primtux8-amd64.iso` mais dans le cas d'une installation de Linux Mint Xfce il faudra choisir la bonne image ; soit, au moment de la rédaction de ce tutoriel, l'image `linuxmint-21.3-xfce-64bit.iso`).

Ensuite sélectionner la clé USB qui a été enfichée dans un port de l'ordinateur.

![](./img_mint/etcher_01.png)

Atendez que l'image de l'OS soit copiée sur la clé USB

![](./img_mint/etcher_02.png)

Attendez de nouveau que l'intégrité de la copie soit vérifiée.

![](./img_mint/etcher_03.png)

La clé USB est prête, vous pouvez la détacher de votre ordinateur pour l'utiliser lors d'installations sur d'autres postes informatiques.

![](./img_mint/etcher_04.png)

## Brancher électriquement la machine et la connecter au réseau

Pour cela choisir un emplacement où on aura assez de prises électrique et au moins une prise ethernet libre utilisable.

## Peaufiner quelques paramètres du BIOS

### Accéder au BIOS de la machine

Pour accéder au BIOS sur la machine testée, il suffit de presser la touche ++del++ pendant le démarrage et on accède alors au BIOS de la machine.

!!! danger "Différents types de BIOS"

    En fonction du type de BIOS, on peut avoir besoin de presser sur une autre touche. Souvent c'est affiché en bas de l'écran de démarrage. Cela peut être ++f1++, ++esc++...


### Désactiver le "secure boot"

Le `secure boot` sera bloquant pour une installation de PrimTux. S'il est activé dans le Bios on pourra tout de même installer LinuxMint mais cela nous oblige à utiliser une nouveau mot de passe pour passer outre ses restrictions. Comme le Secure Boot n'est qu'une invention du couple Microsoft/Intel et que l'on souhaite passer sous du Linux uniquement (pas de dual boot avec du MS Windows), alors il est nettement plus efficace et plus simple de désactiver cette fonctionnalité qui nous sera complètement inutile.

Il convient donc de désactiver cette option dans le BIOS.

![](./img_mint/boot_secure_disabled.jpg)

### Activer le "Boot Menu"

Pour pouvoir accéder à notre clé USB bootable il peut arriver qu'il faille activer le `Boot Menu` (s'il n'est pas déjà activé). Dans le cas de cet ordinateur, cela permettra de presser sur ++f12++ au démarrage pour accéder à la clé USB et lancer l'OS qui y est contenu.

![](./img_mint/boot_menu_enabled.jpg)

### Enregistrer les modifications apportées au BIOS et redémarrer

Quitter le BIOS (souvent cela se fait via la touche ++esc++) et sélectionner l'enregistrement puis le redémarrage. 

!!! tip "Astuce"

    Pour ne pas perdre de temps en redémarrages successifs, vous pouvez placer votre clé USB bootable dans un port libre de l'ordinateur et vous préparer à presser la touche ad-hoc pendant le redémarrage (dans notre cas de figure cela sera la touche ++f12++).

    ![](./img_mint/boot_cle_USB.jpg)




