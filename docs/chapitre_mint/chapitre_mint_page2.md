---
author: Collège Uporu
title: Installation OEM
tags:
    - installation
    - linux
    - OEM
    - reconditionnement
    - Linux Mint
---

## Choix de l'installation 

Lorsqu'on installe classiquement un système d'exploitation sur un ordinateur, on le fait pour soi (personnalisation de l'identifiant et du mot de passe...).

Pour des ordinateurs qui seront cédés à une autre structure, il est plus pratique d'installer une version OEM du système d'exploitation. Comme cela, l'utilisateur final se retrouvera au démarrage sur une interface de personnalisation complète. C'est le même type de configuration que vous avez sur un ordinateur fraîchement acheté en magasin ou sur votre smartphone tout neuf.

Pour cela on va donc : 

1. installer la version OEM
1. mettre la distribution OEM à jour
1. finaliser l'installation en vue de l'expédition à l'utilisateur final

## Démarrage de l'ordinateur sur la clé USB bootable

On place notre [clé USB bootable](./chapitre_mint_page1.md) dans un port libre de l'ordinateur à installer.

On démarre l'ordinateur avec le doigt prêt à appuyer sur la touche ++f12++ et on appuie dessus pour afficher le menu de boot.

On choisit le disque de boot qui correspond à notre clé USB. 
Puis on presse la touche ++enter++

![](./img_mint/boot_cle_USB.jpg)

!!! note "Caractéristiques de la photographie présentée"

    Sur la photographie d'écran présentée on voit que l'ordinateur était déjà équipé d'un système GNU/Linux `Ubuntu`[^1] et que notre clé USB (celle que l'on a correctement sélectionnée) se nomme `SanDisk`.


## Contrôler l'intégrité du disque dur

Nous avons été confrontés à des configurations (partitionnements) des disques durs plutôt hétérogènes et qui pouvaient s'avérer bloquants pour installer Linux (c'était d'ailleurs bloquant pour réinitialiser MSWindows également). Enlever toutes les partitions inutiles des disques durs a donc été une opération indispensable. 

Pour cela, nous avons booté sur la clé USB  et choisi `Start Linux Mint`

![](./img_mint/install_oem_mint_pre_00.png)

Dès le bureau affiché, nous avons lancé GParted et choisi de supprimer toutes les partitions du disque dur (attention à ne pas toucher à la clé USB, mais bien au disque dur de l'ordinateur).

Dans gparted, il faut `démonter` les partitions le cas échéant, puis les `supprimer` et enfin cliquer sur la `coche verte` en haut de la fenêtre pour valider cette reinitialisation du disque dur. 

![](./img_mint/gparted.png)

Ensuite on peut quitter ce mode, en redémarrant la machine pour rebooter sur le mode OEM.


## Installation OEM

La clé USB s'amorce et affiche un menu au sein duquel nous choissisons la version OEM.

![](./img_mint/install_oem_mint_pre_00.png)

Sélectionner la langue française

![](./img_mint/install_oem_mint_pre_01.png)

Laisser la disposition de clavier par défaut, sauf si vous avez des claviers très particuliers dont vous connaissez les caractéristiques. Dans le doute, vous pouvez faire des essais de frappe au clavier dans la fenêtre de saisie proposée.

![](./img_mint/install_oem_mint_pre_02.png)

Sélectionner l'installation des codecs multimédia pour que l'ordi soit pleinement opérationnel (cette validation est là pour bien vous faire comprendre que ces codecs ne sont pas libres de droits et que vous prenez la responsabilité de les installer tout de même).

![](./img_mint/install_oem_mint_pre_03.png)

Choisir une installation complète qui effacera l'intégralité du disque dur pour y placer Linux.

![](./img_mint/install_oem_mint_pre_04.png)

Valider l'écriture sur le disque dur en cliquant sur le bouton `Continuer`

![](./img_mint/install_oem_mint_pre_05.png)

Sélectionner le fuseau horaire (ici la Polynésie française en GMT -10)

![](./img_mint/install_oem_mint_pre_06.png)

Seule information à saisir dans ce formulaire : un mot de passe provisoire pour l'installation OEM. Ici on a choisi `ecole24`. Ce mot de passe nous sera nécessaire pour les étapes de mises à jour du système avant de l'expédier à l'utilisateur final.

![](./img_mint/install_oem_mint_pre_07.png)
![](./img_mint/install_oem_mint_pre_08.png)

On débranche la clé USB et on presse la touche ++enter++. L'ordinateur va redémarrer.

![](./img_mint/install_oem_mint_pre_09.png)


## Personnalisation de l'installation OEM

Au redémarrage de l'ordinateur on se rend compte qu'on est dans la version de personnalisation de l'installation OEM grâce à l'icône en haut à gauche : `Préparer en vue de l'expéditio...`.

![](./img_mint/config_oem_mint_01.png)






### Mises à jour et francisation du système

!!! warning "permettre au gestionnaire de paquet apt de passer le proxy de l'établissement"

    Le cas échéant, il faut éditer le fichier `/etc/apt/apt.conf`

    ```bash
    sudo nano /etc/apt/apt.conf
    ```

    et y ajouter les deux lignes suivante :

    ```
    Acquire::http::proxy "http://<adresseIP>:<portIP>/";
    Acquire::ftp::proxy "ftp://<adresseIP>:<portIP>/";
    ```

    `<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.200.3.45:3128`

    ![](./img_mint/ajout_IP_Proxy.png)

On met à jour le système (on note un point rouge sur l'icône corerspondant en bas à droite). Les différentes captures d'écran qui suivent ne sont pas commentées individuellements. Elles montrent juste qu'il peut y avoir plusieurs étapes successives pour mettre à jour compètement le système. Ces captures ayant été faites à une date donnée, il est probable que les propositions de mises à jour soient sensiblement différentes si l'installation se déroule sur une autre date ultérieure.

![](./img_mint/config_oem_mint_02.png)
![](./img_mint/config_oem_mint_03.png)
![](./img_mint/config_oem_mint_04.png)
![](./img_mint/config_oem_mint_05.png)
![](./img_mint/config_oem_mint_06.png)
![](./img_mint/config_oem_mint_07.png)
![](./img_mint/config_oem_mint_08.png)
![](./img_mint/config_oem_mint_09.png)
![](./img_mint/config_oem_mint_10.png)

!!! warning "supprimer les références au proxy"

    Il faut éditer le fichier `/etc/apt/apt.conf`

    ```bash
    sudo nano /etc/apt/apt.conf
    ```

    et en supprimer les deux lignes suivante :

    ```
    Acquire::http::proxy "http://<adresseIP>:<portIP>/";
    Acquire::ftp::proxy "ftp://<adresseIP>:<portIP>/";
    ```

    `<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.21.0.2:3128`

Comme recommandé dans la capture d'écran précédente, on choisit le redémarrage de l'ordinateur pour fixer toutes les mises à jour réalisées.

![](./img_mint/config_oem_mint_11.png)




## Validation de l'OEM et arrêt



En cliquant sur le bouton ad-hoc en haut à gauche de la fenêtre, nous activons la version prête pour l'expédition à l'utilisateur final. 

![](./img_mint/config_oem_mint_12.png)

Le système demande la saisie du mot de passe OEM

![](./img_mint/config_oem_mint_13.png)

Le système confirme que le prochain démarrage sera une version pour l'utilisateur final

![](./img_mint/config_oem_mint_14.png)

A ce moment, on peut `Eteindre` l'ordinateur, le débrancher et le préparer pour l'expédition physique à l'utilisateur final.

![](./img_mint/config_oem_mint_15.png)


[^1]: Je ne vais pas le cacher, j'ai commencé par installer plusieurs postes en même temps, dont ce poste de démonstration de LinuxMint. Or il s'avère que j'ai, très vraisemblablement, saisi un mot de passe erroné et j'ai été contraint de réitérer l'installation. Outre le fait que je me suis senti très bête lorsque je me suis rendu compte de cette erreur, cela ne fait que confirmer le caractère crucial de l'étape de choix du mot de passe.