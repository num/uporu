---
author: Collège Uporu
title: Logiciels de PrimTux sur LinuxMint
tags:
    - PrimTux
    - LinuxMint
    - Linux
    - standalone edition
    - logiciels
    - proxy
---

## Les logiciels de PrimTux sur un ordinateur sous LinuxMint

Après plusieurs jours de démonstration de deux ordinateurs sous PrimTux 8 et LinuxMint Xfce edition, nous avons relevé les remarques suivantes :

- les logigiels de PrimTux seraient intéressants pour des professeurs encadrants des élèves à besoins particuliers ;
- l'interface de PrimTux risque de faire un peu trop "enfantine" et provoquer un rejet chez certains élèves de collège ;
- LinuxMint est intéressante car rapide et possédant les logiciels de bureautique habituels (LibreOffice normal...).



## Préparation de l'ordinateur pour pouvoir lancer le script à travers le proxy de l'établissement

Le script utilie la commande apt et la commande wget.

Il faut donc paramétrer le passage par le proxy pour ces deux commandes, à minima.

??? warning "permettre au gestionnaire de paquet apt de passer le proxy de l'établissement"

    Cette modification a pu [être réalisée auparavant](./chapitre_mint_page2.md), elle ne sera donc pas à refaire une seconde fois.

    Le cas échéant, il faut éditer le fichier `/etc/apt/apt.conf`

    ```bash
    sudo nano /etc/apt/apt.conf
    ```

    et y ajouter les deux lignes suivante :

    ```
    Acquire::http::proxy "http://<adresseIP>:<portIP>/";
    Acquire::ftp::proxy "ftp://<adresseIP>:<portIP>/";
    ```

    `<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.21.0.2:3128`

    ![](./img_mint/ajout_IP_Proxy.png)

!!! warning "permettre à la commande wget de passer le proxy de l'établissement"

        Le cas échéant, il faut éditer le fichier `/etc/wgetrc

    ```bash
    sudo nano /etc/wgetrc
    ```

    trouver les lignes suivante :

    ```
    # You can set up other headers, like Accept-Language.  Accept-Language
    # is *not* sent by default.
    #header = Accept-Language: en

    # You can set the default proxies for Wget to use for http, https, and ftp.
    # They will override the value in the environment.
    #https_proxy = http://proxy.yoyodyne.com:18023/
    #http_proxy = http://proxy.yoyodyne.com:18023/
    #ftp_proxy = http://proxy.yoyodyne.com:18023/

    # If you do not want to use proxy at all, set this to off.
    #use_proxy = on
    ```

    et les modifier comme suit (décommenter 4 lignes et corriger les adresses IP du proxy) ;   `<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.21.0.2:3128`:


    ```
    # You can set up other headers, like Accept-Language.  Accept-Language
    # is *not* sent by default.
    #header = Accept-Language: en

    # You can set the default proxies for Wget to use for http, https, and ftp.
    # They will override the value in the environment.
    https_proxy = http://<adresseIP>:<portIP>/
    http_proxy = http://<adresseIP>:<portIP>/
    ftp_proxy = http://<adresseIP>:<portIP>/

    # If you do not want to use proxy at all, set this to off.
    use_proxy = on

    ```

    ![](./img_mint/wgetrc_proxy.png)    


## Télécharger le fichier d'installation et la documentation

[La documentation pour l'installation de la version Standalone de PrimTux](https://primtux.fr/primtux-standalone.pdf) contient les liens et la procédure pour installer les logiciels de PrimTux dans la Linux Mint 21

Même si la quantité de téléchargement avoisine les 4Go, l'installation se fait toute seule avec uniquement deux commandes à lancer successivement dans le terminal.

## Ce que cela apporte d'installer PrimTux standalone edition

- Si les ordinateurs ne sont pas connectés à internet (panne, cablage en retard...) il est possible de faire utiliser l'encyclopédie Vikidia par les élèves puisqu'une version hors ligne est installée sur les postes[^1] ;
- la version de LibreOffice est tout ce qu'il y a de plus classique, comme sur tous les autres ordinateurs du collège ;
- on peut choisir de n'afficher qu'un lien (raccourci) vers le handy-menu du cycle 3 pour que les élèves à besoins particuliers de collège puissent y accéder directement.


## Installations complémentaires

Pour s'entraîner et passer les certifications [Pix](https://pix.fr) par exemple, les ordinateurs doivent être équipés de The Gimp et de ShotCut en plus de la suite bureautique LibreOffice préinstallée. 

Pour The Gimp et Shotcut une simple ligne de commande est suffisante (dans le terminal) :

```bash
sudo apt install gimp shotcut
```

[BDnF](https://bdnf.bnf.fr/fr) peut s'avérer également intéressante.

Ensuite on peut ajouter des raccourcis directs sur le bureau et/ou des onglets au démarrage de Firefox vers [L'ENT](https://nati.pf), [e-sidoc du collège](https://9840234g.esidoc.fr/), [pronote](https://9840234g.index-education.net/pronote/), [pix](https://pix.fr), les [entraînements ASSR2](https://e-assr.education-securite-routiere.fr/preparer/assr/2/ASSR2)...

## Intégration au domaine

Pour [intégrer ces postes au domaine, il faut se référer à la documentation](../tutos_eole/integrations.md).

[^1]: Mais une erreur empêche le lancement de Kiwix (librairie libkiwix.so.10). Cela sera à corriger si possible.


