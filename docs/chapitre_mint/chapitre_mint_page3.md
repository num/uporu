---
author: Collège Uporu
title: Premier démarrage
tags:
    - lancement
    - premier contact
    - débutant
    - LinuxMint
    - Linux
---




## Branchement et premier lancement de LinuxMint

Les trois premières étapes ne devraient pas nécessiter d'intervention de votre part, si ce n'est de cliquer sur le bouton `Continuer` en bas à droite des écrans.

![](./img_mint/demarrage_client_mint_01.png)
![](./img_mint/demarrage_client_mint_02.png)
![](./img_mint/demarrage_client_mint_03.png)

On arrive à l'étape de personnalisation (identifiant et mot de passe). **N'oubliez pas de noter précautionneusement vos informations saisies.**

1. Saisissez votre **nom**
1. Je vous conseille de laisser l'installateur choisir le **nom de l'ordi** à moins que vous sachiez quoi faire
1. Un **nom d'utilisateur** est proposé. Vous pouvez le laisser tel quel s'il vous convient.
1. Saisissez (deux fois le même) votre **mot de passe**. L'interface vous dira s'il est suffisament sécurisé et fonctionnel.
1. Je conseille toujours de garder **`Demander mon mot de passe pour ouvrir la session`** pour des raisons de sécurité mais également pour être certain de vous souvenir de ce mot de passe nécessaire lors des installations de nouveaux logiciels notamment.

![](./img_mint/demarrage_client_mint_04.png)
![](./img_mint/demarrage_client_mint_05.png)



## Utilisation de votre nouveau Linux

Au démarrage de votre ordinateur vous avez accès à ce bureau.

![](./img_mint/demarrage_client_mint_06.png)

En cliquant sur l'icône `LinuxMint` tout en bas à gauche de l'écran vous pourrez afficher le menu des logiciels et vous rendre notamment dans la logithèque pour découvrir et installer de nouveaux logiciels.

![](./img_mint/logitheque_linux_mint.png)

Tous les logiciels présentés dans la Logithèque sont libres et gratuits. Leur description permet de comprendre quelles sont leurs fonctionnalités.

L'annuaire de logiciels libre [Framalibre](https://framalibre.org) vous permettra de découvrir les logiciels libres correspondants à vos besoins.

Pour information, voici les logiciels libres recommandés par les rédacteurs de cette notice :

- [Logiciels recommandés par un professeur](https://phardouin.github.io/recommandations-du-professeur-patrice-hardouin/)