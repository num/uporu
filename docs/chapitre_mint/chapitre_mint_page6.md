---
author: Collège Uporu
title: Installation classique
tags:
    - LinuxMint
    - Linux
    - proxy
    - mise à jour
    - clé de boot
    - installation
    - disque dur
    - formatage
---

## Choix de l'installation 

Lorsqu'on installe classiquement un système d'exploitation sur un ordinateur, on le fait pour soi (personnalisation de l'identifiant et du mot de passe...).

Pour des ordinateurs qui seront intégrés au domaine du serveur pédagogique (serveur EOLE dans notre cas), c'est cette installation que nous devons réaliser avec un identifiant et un mot de passe qui sera le même pour tous les postes à déployer.

Pour cela on va donc : 

1. installer la version classique
1. mettre la distribution à jour
1. intégrer la distribution au domaine

## S'assurer des pré-requis à l'installation

Pour cela se référer à [la page ad-hoc du tutoriel](./chapitre_mint_page1.md).

### Démarrage de l'ordinateur sur la clé USB bootable

On place notre [clé USB bootable](./chapitre_mint_page1.md) dans un port libre de l'ordinateur à installer.

On démarre l'ordinateur avec le doigt prêt à appuyer sur la touche ++f12++ et on appuie dessus pour afficher le menu de boot.

On choisit le disque de boot qui correspond à notre clé USB. 
Puis on presse la touche ++enter++

![](./img_mint/boot_cle_USB.jpg)

!!! note "Caractéristiques de la photographie présentée"

    Sur la photographie d'écran présentée on voit que l'ordinateur était déjà équipé d'un système GNU/Linux `Ubuntu`[^1] et que notre clé USB (celle que l'on a correctement sélectionnée) se nomme `SanDisk`.


### Contrôler l'intégrité du disque dur

Nous avons été confrontés à des configurations (partitionnements) des disques durs plutôt hétérogènes et qui pouvaient s'avérer bloquants pour installer Linux (c'était d'ailleurs bloquant pour réinitialiser MSWindows également). Enlever toutes les partitions inutiles des disques durs a donc été une opération indispensable. 

Pour cela, nous avons booté sur la clé USB  et choisi `Start Linux Mint`

![](./img_mint/boot_install_LM.png)

Dès le bureau affiché, nous avons lancé GParted et choisi de supprimer toutes les partitions du disque dur (attention à ne pas toucher à la clé USB, mais bien au disque dur de l'ordinateur).

Dans gparted, il faut `démonter` les partitions le cas échéant, puis les `supprimer` et enfin cliquer sur la `coche verte` en haut de la fenêtre pour valider cette reinitialisation du disque dur. 

![](./img_mint/gparted.png)

Ensuite on continue pour installer le système d'exploitation sur un disque propre


## Installation classique

Il n'y a qu'une icône sur le bureau pour installer Linux Mint. On clique dessus pour lancer la procédure.

![](./img_mint/Install_Linux_Mint_icon.png)

Sélectionner la langue française

![](./img_mint/install_oem_mint_pre_01.png)

Laisser la disposition de clavier par défaut, sauf si vous avez des claviers très particuliers dont vous connaissez les caractéristiques. Dans le doute, vous pouvez faire des essais de frappe au clavier dans la fenêtre de saisie proposée.

![](./img_mint/install_oem_mint_pre_02.png)

Sélectionner l'installation des codecs multimédia pour que l'ordi soit pleinement opérationnel (cette validation est là pour bien vous faire comprendre que ces codecs ne sont pas libres de droits et que vous prenez la responsabilité de les installer tout de même).

![](./img_mint/install_oem_mint_pre_03.png)

Choisir une installation complète qui effacera l'intégralité du disque dur pour y placer Linux.

![](./img_mint/install_oem_mint_pre_04.png)

Valider l'écriture sur le disque dur en cliquant sur le bouton `Continuer`

![](./img_mint/install_oem_mint_pre_05.png)

Sélectionner le fuseau horaire (ici la Polynésie française en GMT -10)

![](./img_mint/install_oem_mint_pre_06.png)

Les informations à saisir dans ce formulaire : un identifiant administrateur (`prof`), un mot de passe et le nom de la machine. Pour nous cela sera L23-S220-P02 (pour LinuxMint23, salle 220 et poste n°2 dans cette salle) pour correspondre aux exigences au serveur pédagogique.

![](./img_mint/prof_nom_machine_install_classique.png)
![](./img_mint/install_oem_mint_pre_08.png)

On débranche la clé USB et on presse la touche ++enter++. L'ordinateur va redémarrer.

![](./img_mint/install_oem_mint_pre_09.png)


## Finalisation de l'installation classique

### Mises à jour et francisation du système

!!! warning "permettre au gestionnaire de paquet apt de passer le proxy de l'établissement"

    Le cas échéant, il faut éditer le fichier `/etc/apt/apt.conf`

    ```bash
    sudo nano /etc/apt/apt.conf
    ```

    et y ajouter les deux lignes suivante :

    ```
    Acquire::http::proxy "http://<adresseIP>:<portIP>/";
    Acquire::ftp::proxy "ftp://<adresseIP>:<portIP>/";
    ```

    `<adresseIP>:<portIP>` correspond à l'adresse et au port du proxy de votre établissement. Par exemple, nous pourrions avoir quelque chose du genre `10.21.0.2:3128`

    ![](./img_mint/ajout_IP_Proxy.png)

On met à jour le système. Pour cela suivre la procédure qui se déclenche lorsqu'on clique sur le petit bouclier (avec un point rouge signifiant qu'il y a des mises à jour de disponibles) qui se trouve en bas à droite du bureau.

![](./img_mint/mises_a_jour_install_classique.png)

Ensuite on redémarre l'ordinateur ce qui permet de fixer les mises à jours importantes qui ont pu être installées. Le redémarrage de l'ordinateur n'est pas systématique suite à une mise à jour, cela n'est normalement nécessaire qu'en cas de nouveau noyau du système ou assimilé. Mais LinuxMint vous préviendra si un redémarrage doit être fait.






### Installations complémentaires



On peut choisir d'installer des logiciels complémentaires.

Pour un lot d'ordinateurs (5 postes), le choix a été fait par les enseignants d'installer [PrimTux Standalone](./chapitre_mint_page5.md).

D'autres logiciels peuvent être intéressants à placer comme shotcut et gimp qui sont recommandés pour les entraînements et les certifications à [Pix](https://pix.fr) Pour cela on peut utiliser la logithèque ou une simple commande dans une console texte : 

``` 
sudo apt install gimp shotcut

```

## Intégration au domaine


Pour intégrer un poste GNU/Linux au domaine, on peut se référer à la [documentation officielle d'EOLE](../tutos_eole/integrations.md).

[^1]: Je ne vais pas le cacher, j'ai commencé par installer plusieurs postes en même temps, dont ce poste de démonstration de LinuxMint. Or il s'avère que j'ai, très vraisemblablement, saisi un mot de passe erroné et j'ai été contraint de réitérer l'installation. Outre le fait que je me suis senti très bête lorsque je me suis rendu compte de cette erreur, cela ne fait que confirmer le caractère crucial de l'étape de choix du mot de passe.