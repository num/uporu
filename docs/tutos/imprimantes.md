---
author: Collège Uporu
title: Utiliser les imprimantes
tags:
    - imprimante
    - Windows
    - Linux
    - pilote
    - driver
    - scan
---

## Références des photocopieurs Toshiba

- Le photocopieur professeurs possède la référence : e-STUDIO3508A
- Le photocopieur administratif possède la référence : e-STUDIO3515AC

## Pilotes à installer

Le pilote à utiliser se nomme  `TOSHIBA e-STUDIO Universal Printer Driver 2` et la dernière version à la date de rédaction de ce tutoriel était `v7.222.5412.176 (82MB)`.

On trouve les pilotes sur le [site des drivers Toshiba](https://www.toshibatec.eu/support/drivers/SearchDriver?searchString=e-STUDIO3515AC) (le lien donné présente un pilote qui est valable pour la machine admin notamment).

## Pilotes pour GNU/Linux

Sur le [site des drivers Toshiba](https://www.toshibatec.eu/support/drivers/SearchDriver?searchString=e-STUDIO3515AC) on peut noter la présence de pilote CUPS qui peuvent être utilisés sous GNU/Linux.

A noter toutefois que lors des démonstrations sur des postes LinuxMint et PrimTux (distributions basées sur Ubuntu 22.04) le photocopieur prof a été reconnu d'office sans nécessiter aucune installation manuelle de pilote Toshiba.

## Particularité du scan sur le photcopieur Admin

Les utilisateurs se connectent au photocopieur avec leur couple id/mdp et scannent leurs documents. Les documents se retrouvent alors sur le disque réseau de l'utilisateur, sur le serveur administratif. 

Pour accéder directement à cet espace, un raccourci peut être mis en place sur le poste de l'agent en question, raccourci qui pointe vers le disque réseau réceptionnant les scans.
