---
author: Collège Uporu
title: Créer des clés de boot système
tags:
    - boot
    - clé USB
    - Linux
    - Windows
    - installation
    - réparation
    - système d'exploitation
---

## Clé de boot avec Balena Etcher

!!! tip "Créer une clé USB bootable en moins de 4 minutes"

    <iframe title="Création d'une clé USB de boot GNU/Linux" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/44b430b4-a0a4-4246-ba0a-771a78b68025" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


Tout est expliqué dans [les prérequis au reconditionnement de postes sous LinuxMint et PrimTux, avec Balena Etcher](../chapitre_mint/chapitre_mint_page1.md#telecharger-et-installer-balena-etcher).


## Clé de boot multiOS avec Ventoy

Ventoy est très pratique pour créer des clé USB multi-boot.
On peut ainsi y placer les images iso de Debian Facile 12, Linux Mint XFCE 22, PrimTux 8, MX-Linux 23.4 en version 64 bits et 32 bits pour les ordinosaures encore présents sur l'établissement. On peut y ajouter les images iso d'installation de MS Windows 10 et 11 et les tenir à jour facilement (il n'y a qu'à remplacer l'image iso qui est à la racine de la clé Ventoy).

Pour créer une clé USB bootable (ou un disque USB bootable) Ventoy, je vous renvoie vers [la page officielle de Ventoy](https://www.ventoy.net/).
Vosu pourrez [télécharger Ventoy directement sur le dépôt Sourceforge du projet](https://sourceforge.net/projects/ventoy/files/). Si vous choisissez de télécharger le Ventoy pour MS Windows, une fois téléchargé et décompressé, lancez l'exécutable Ventoy2Disk.exe pour créer votre clé USB Ventoy bootable. Ensuite vous ajouterez vos images iso à la racine de votre clé Ventoy nouvellement formatée.

Personnellement, j'ai ajouté un répertoire (à la racine de la clé Ventoy) contenant l'installateur de [Balena Etcher](../chapitre_mint/chapitre_mint_page1.md#telecharger-et-installer-balena-etcher) afin de pouvoir créer une clé USB bootable spécifique à un seul OS pour les machines qui ne supporteraient pas Ventoy (il y en a quelques unes).




## Images iso de MSWindows

Il faut télécharger le fichier .iso de la distribution et le fichier exécutable (.exe) permettant de créer une clé USB bootable.

Tout se passe sur le site de Microsoft :

- [Clé de Boot pour Windows 11](https://www.microsoft.com/fr-fr/software-download/windows11)
- [Clé de Boot pour Windows 10](https://www.microsoft.com/fr-fr/software-download/windows10)

## Autre procédure pour créer une clé de boot windows depuis Linux

Si vous souhaitez une procédure différente de Balena Etcher ou Ventoy pour vos clés bootable MS Windows, après avoir téléchargé l'image iso ci-avant, il est possible de [créer des clés de boot MSWindows depuis un système GNU/Linux depuis `Gparted` ou une interface de type `Rufus`](https://doc.ubuntu-fr.org/tutoriel/installer_windows_boot_usb)