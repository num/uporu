---
author: Collège Uporu
title: 🏡 Accueil
---

# Ressources pour l'utilisation du numérique au collège Uporu

## Documentations tout public

- [Prendre en main un ordinateur livré avec LinuxMint](./chapitre_mint/chapitre_mint_page3.md) ;
- [Découvrir les systèmes Linux Mint et PrimTux](./tutos_linux/decouverte_linux_distri.md).

## Documentations techniques

- Tutoriels génériques :
    - [Installer les imprimantes réseau](./tutos/imprimantes.md) ;
    - [Créer des clés de boot pour les systèmes d'exploitation](./tutos/cle_boot.md).
- Tutoriels GNU/Linux :
    - Gérer le [reconditionnement d'ordinateurs sous GNU/Linux](./chapitre_mint/chapitre_mint_page0.md) ;
    - [Installer Debian 12 (même procédure pour MX-Linux)](./tutos_linux/install_debian12.md) ;
    - [Installer le client Pronote sous Linux avec Wine](./tutos_linux/install_pronote_linux.md) ;
    - [Installer Adobe Digital Edition sous Linux avec Wine](./tutos_linux/install_ade_linux.md) ;
    - [Créer des clés de boot pour les systèmes d'exploitation GNU/Linux](./chapitre_mint/chapitre_mint_page1.md) ;
    - [Installer Geogebra 5](./tutos_linux/install_geogebra_5.md) ;
    - [Installer Scratch 3](./tutos_linux/install_scratch_3.md).
- Gestion du serveur EOLE 2.8 :
    - [Intégration des postes MS Windows et GNU/Linux au domaine](./tutos_eole/integrations.md)
